// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "Types.generated.h"

UENUM(BlueprintType)
enum class EMovementState : uint8
{
	RunState UMETA(DisplayName = "RunState"),
	SprintState UMETA(DisplayName = "SprintState"),
	CrouchState UMETA(DisplayName = "CrouchState"),
	AimState UMETA(DisplayName = "AimState"),
	AimCrouchState UMETA(DisplayName = "AimCrouchState")
};

USTRUCT(BlueprintType)
struct FTPSCharacterStateSpeed
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement | StateSpeed")
	float RunSpeed = 600.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement | StateSpeed")
	float SprintSpeed = 900.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement | StateSpeed")
	float CrouchSpeed = 400.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement | StateSpeed")
	float AimSpeed = 200.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement | StateSpeed")
	float AimCrouchSpeed = 100.0f;
};

UCLASS()
class TPS_API UTypes : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
};
